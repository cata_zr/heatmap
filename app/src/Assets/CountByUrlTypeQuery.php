<?php

namespace App\Assets;

use Symfony\Component\Validator\Constraints as Assert;

class CountByUrlTypeQuery
{
    /**
     * @Assert\NotBlank
     * @Assert\Choice({"product", "category", "static-page", "checkout", "homepage"})
     */
    public string $urlType;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     */
    public string $fromDate;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     */
    public string $toDate;
}