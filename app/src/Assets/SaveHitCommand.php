<?php

namespace App\Assets;

use Symfony\Component\Validator\Constraints as Assert;

class SaveHitCommand
{
    /**
     * @Assert\NotBlank
     * @Assert\Url
     */
    public string $url;

    /**
     * @Assert\NotBlank
     * @Assert\Choice({"product", "category", "static-page", "checkout", "homepage"})
     */
    public string $urlType;

    /**
     * @Assert\NotBlank
     * @Assert\Email
     */
    public string $email;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     */
    public string $autostamp;
}