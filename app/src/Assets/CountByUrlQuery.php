<?php

namespace App\Assets;

use Symfony\Component\Validator\Constraints as Assert;

class CountByUrlQuery
{
    /**
     * @Assert\NotBlank
     * @Assert\Url
     */
    public string $url;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     */
    public string $fromDate;

    /**
     * @Assert\NotBlank
     * @Assert\DateTime
     */
    public string $toDate;
}