<?php

namespace App\Service;

use App\Repository\HitsRepository;

class HitsService
{
    private const PRE_MATCH_COUNT = 10;

    private HitsRepository $hitsRepository;

    public function __construct(HitsRepository $hitsRepository)
    {
        $this->hitsRepository = $hitsRepository;
    }

    public function getCustomersForJourney($data): array
    {
        // sort them desc
        uasort($data, function($a, $b) {
            if ($a["Autostamp"] === $b["Autostamp"]) {
                return 0;
            }

            return $a['Autostamp'] < $b['Autostamp'] ? 1 : -1;
        });

        $urls = array_column($data, "Url");
        $urlsHash = md5(json_encode($urls));
        $urlsCount = count($urls);

        $preMatchUrls = array_slice($urls, 0, self::PRE_MATCH_COUNT);

        $possibleCustomers = $this->hitsRepository->getCustomersByUrls($preMatchUrls);

        $customers = [];
        foreach ($possibleCustomers as $email) {
            $matchUrls = $this->hitsRepository->getPreviousUrls($email, $urls[0], $urlsCount);

            if (md5(json_encode($matchUrls)) === $urlsHash) {
                $customers[] = $email;
            }
        }

        return $customers;
    }
}