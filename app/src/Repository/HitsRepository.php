<?php

namespace App\Repository;

use App\Assets\CountByUrlQuery;
use App\Assets\CountByUrlTypeQuery;
use App\Assets\SaveHitCommand;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\ParameterType;

class HitsRepository
{
    private Connection $conn;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    public function getCustomersByUrls(array $urls): array
    {
        try {
            $result = $this->conn->executeQuery(
                "select distinct Email from hits where Url in (?)",
                [$urls],
                [Connection::PARAM_STR_ARRAY]
            );

            if ($result->rowCount()) {
                return (array) $result->fetchFirstColumn();
            }
        } catch (\Throwable $e) {}

        return [];
    }

    public function saveHit(SaveHitCommand $hit): bool
    {
        try {
            $this->conn->insert("hits", [
                "HitId" => null,
                "Url" => $hit->url,
                "UrlType" => $hit->urlType,
                "Email" => $hit->email,
                "Autostamp" => $hit->autostamp
            ]);

            $insertedId = $this->conn->lastInsertId();

            if ($insertedId) {
                return true;
            }
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); exit;
        }

        return false;
    }

    public function countByUrl(CountByUrlQuery $payload): int
    {
        try {
            return (int) $this->conn->fetchOne(
                "select count(HitId) from hits where Url = :url and Autostamp between :fromdate and :todate",
                [
                    "url" => $payload->url,
                    "fromdate" => $payload->fromDate,
                    "todate" => $payload->toDate
                ]
            );
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); exit;
        }
    }

    public function countByUrlType(CountByUrlTypeQuery $payload): int
    {
        try {
            return (int) $this->conn->fetchOne(
                "select count(HitId) from hits where UrlType = :urltype and Autostamp between :fromdate and :todate",
                [
                    "urltype" => $payload->urlType,
                    "fromdate" => $payload->fromDate,
                    "todate" => $payload->toDate
                ]
            );
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); exit;
        }
    }

    public function getJourneyForEmail(string $email): array
    {
        try {
            $q = $this->conn->prepare("select Autostamp, Url from hits where Email = :email order by Autostamp asc");
            $q->bindParam('email', $email);

            $result = $q->executeQuery();

            if ($result->rowCount()) {
                return (array) $result->fetchAllAssociative();
            }
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); exit;
        }
    }

    public function getCustomersByHash(string $hash)
    {
        try {
            return (array) $this->conn->fetchFirstColumn(
                "select Email from hits where JourneyHash = :hash group by Email",
                [
                    "hash" => $hash
                ]
            );
        } catch (\Throwable $e) {
            var_dump($e->getMessage()); exit;
        }
    }

    public function getPreviousUrls(string $email, string $refUrl, int $howMany): array
    {
        try {
            $result = $this->conn->executeQuery(
                "
                        select h2.Url
                        from hits h1
                        inner join hits h2
                            on h1.Email = h2.Email
                            and h1.Autostamp >= h2.Autostamp
                        where h1.Email = ?
                          and h1.Url = ?
                          and h2.Url is not null
                        group by h2.Url, h2.AutoStamp
                        order by h2.AutoStamp desc
                        limit ?
                    ",
                [$email, $refUrl, $howMany],
                [
                    ParameterType::STRING,
                    ParameterType::STRING,
                    ParameterType::INTEGER
                ]
            );

            if ($result->rowCount()) {
                return (array) $result->fetchFirstColumn();
            }
        } catch (\Throwable $e) {}

        return [];
    }
}