<?php

namespace App\Controller;

use App\Assets\CountByUrlQuery;
use App\Assets\CountByUrlTypeQuery;
use App\Assets\SaveHitCommand;
use App\Repository\HitsRepository;
use App\Service\HitsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HitsController extends AbstractController
{
    private SerializerInterface $serializer;
    private ValidatorInterface $validator;
    private HitsService $hitsService;
    private HitsRepository $hitsRepository; //TODO: extract interface

    public function __construct(
        ValidatorInterface $validator,
        HitsService $hitsService,
        HitsRepository $hitsRepository
    )
    {
        $this->serializer =  new Serializer([new ObjectNormalizer()], [new JsonEncoder()]);
        $this->validator = $validator;
        $this->hitsService = $hitsService;
        $this->hitsRepository = $hitsRepository;
    }

    public function getJourneyCustomersAction(Request $request): Response
    {
        try {
            $data = json_decode($request->getContent(), true);

            if (json_last_error() !== JSON_ERROR_NONE) {
                return new JsonResponse(
                    ["title" => "Invalid json"],
                    Response::HTTP_BAD_REQUEST
                );
            }

            $errors = $this->validator->validate($data, [
                new Type('array'),
                new Count(['min' => 1]),
                new All([
                    new Collection([
                        "fields" => [
                            "Autostamp" => [new NotBlank(), new DateTime()],
                            "Url" => [New NotBlank(), new Url()]
                        ]
                    ])
                ])
            ]);

            if ($errors->count() > 0) {
                return new JsonResponse(
                    $this->formatValidationErrors($errors),
                    Response::HTTP_BAD_REQUEST
                );
            }

            $emails = $this->hitsService->getCustomersForJourney($data);

            return new JsonResponse(
                $emails,
                Response::HTTP_OK
            );
        } catch (\Throwable $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function getCustomerJourneyAction(Request $request): Response
    {
        try {
            $email = $request->query->get("email");

            $errors = $this->validator->validate($email, [new NotBlank(), new Email()]);;
            if ($errors->count() > 0) {
                return new JsonResponse(
                    $this->formatValidationErrors($errors),
                    Response::HTTP_BAD_REQUEST
                );
            }

            $journey = $this->hitsRepository->getJourneyForEmail($email);

            return new JsonResponse(
                $journey,
                Response::HTTP_OK
            );
        } catch (\Throwable $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function countByUrlAction(Request $request): Response
    {
        try {
            /** @var CountByUrlQuery $payload */
            $payload = $this->serializer->deserialize(
                json_encode($request->query->all()),
                CountByUrlQuery::class,
                'json',
            );

            $errors = $this->validator->validate($payload);
            if ($errors->count() > 0) {
                return new JsonResponse(
                    $this->formatValidationErrors($errors),
                    Response::HTTP_BAD_REQUEST
                );
            }

            $count = $this->hitsRepository->countByUrl($payload);

            return new JsonResponse(
                ["hits" => $count],
                Response::HTTP_OK
            );
        } catch (\Throwable $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function countByUrlTypeAction(Request $request): Response
    {
        try {
            /** @var CountByUrlTypeQuery $payload */
            $payload = $this->serializer->deserialize(
                json_encode($request->query->all()),
                CountByUrlTypeQuery::class,
                'json',
            );

            $errors = $this->validator->validate($payload);
            if ($errors->count() > 0) {
                return new JsonResponse(
                    $this->formatValidationErrors($errors),
                    Response::HTTP_BAD_REQUEST
                );
            }

            $count = $this->hitsRepository->countByUrlType($payload);

            return new JsonResponse(
                ["hits" => $count],
                Response::HTTP_OK
            );
        } catch (\Throwable $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    public function postHitAction(Request $request): Response
    {
        try {
            /** @var SaveHitCommand $payload */
            $payload = $this->serializer->deserialize(
                $request->getContent(),
                SaveHitCommand::class,
                'json',
            );

            $errors = $this->validator->validate($payload);
            if ($errors->count() > 0) {
                return new JsonResponse(
                    $this->formatValidationErrors($errors),
                    Response::HTTP_BAD_REQUEST
                );
            }

            $hit = $this->hitsRepository->saveHit($payload);

            if (!$hit) {
                return new JsonResponse(
                    ["error" => "could not save hit"],
                    Response::HTTP_INTERNAL_SERVER_ERROR
                );
            }

            return new JsonResponse(
                "ok",
                Response::HTTP_CREATED
            );
        } catch (\RuntimeException $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_BAD_REQUEST
            );
        } catch (\Throwable $e) {
            return new JsonResponse(
                ["error" => $e->getMessage()],
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }
    }

    private function formatValidationErrors($violations): array
    {
        $error = [
            "error" => "Bad Request",
            "violations" => []
        ];

        foreach ($violations as $item) {
            $error["violations"][] = [
                "property" => $item->getPropertyPath(),
                "message" => $item->getMessage()
            ];
        }

        return $error;
    }
}