<?php

declare(strict_types=1);

namespace HeatMap\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220416193115 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS hits (
                HitId int(10) NOT NULL AUTO_INCREMENT,
                Url varchar(255) NOT NULL,
                UrlType varchar(20) NOT NULL,
                Email varchar(100) NOT NULL,
                AutoStamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                PRIMARY KEY(HitId),
                INDEX IX_email (Email),
                INDEX ix_autostamp (Autostamp),
            ) Engine=InnoDB;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DROP TABLE IF EXISTS hits;");
    }
}
